package at.mager.games.wintergame;

import java.util.Random;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;

public class ShootingStar {
	private float x, y;
	private float time;
	private Random randomPosition;
	private Random randomTime;
	private Polygon source;

	public ShootingStar(float x, float y) {
		super();

		newStar();

	}

	private void newStar() {

		this.source = new Polygon();
		source.addPoint(100, 100);
		source.addPoint(105, 115);
		source.addPoint(120, 115);
		source.addPoint(109, 125);
		source.addPoint(115, 140);
		source.addPoint(100, 130);
		source.addPoint(85, 140);
		source.addPoint(91, 125);
		source.addPoint(60, 120);
		source.addPoint(20, 130);
		source.addPoint(30, 120);
		source.addPoint(20, 113);
		source.addPoint(65, 108);
		source.addPoint(95, 115);

		this.randomPosition = new Random();
		this.x = randomPosition.nextInt(350);
		this.source.setCenterX(this.x);
		this.y = randomPosition.nextInt(150);
		this.source.setCenterY(this.y);

		this.randomTime = new Random();
		this.time = randomTime.nextInt(40000);

	}

	public void update(GameContainer gc, int delta) {

		if (x <= 860) {
			this.x = this.x + (2 * delta);
			this.source.setCenterX(this.x);
		} else {
			this.time -= delta;
			if (this.time <= 0) {
				newStar();
			}
		}
	}

	public void render(Graphics graphics) {
		graphics.fill(this.source);
	}

}
