package at.mager.games.wintergame;

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	private CircleActor ca1;
	private RectActor ra1;
	private OvalActor oa1;
	private ShootingStar ss;
	private Snowman sm;
	private List<SnowActor> snowactor;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//this.ca1.render(graphics);
		//this.ra1.render(graphics);
		//this.oa1.render(graphics);
		this.sm.render(graphics);
		this.ss.render(graphics);
		for (SnowActor s : this.snowactor) {
			s.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.ca1 = new CircleActor(400, 100);
		this.ra1 = new RectActor(100, 50);
		this.oa1 = new OvalActor(100, 400);
		this.ss = new ShootingStar(400, 200);
		this.sm = new Snowman();
		this.snowactor = new ArrayList<>();
		for(int i = 0; i < 50; i++) {
			this.snowactor.add(new SnowActor(0));
			this.snowactor.add(new SnowActor(1));
			this.snowactor.add(new SnowActor(2));
		}

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		this.ca1.update(gc, delta);
		this.ra1.update(gc, delta);
		this.oa1.update(gc, delta);
		this.ss.update(gc, delta);
		this.sm.update(gc, delta);
		for (SnowActor s : this.snowactor) {
			s.update(gc, delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
