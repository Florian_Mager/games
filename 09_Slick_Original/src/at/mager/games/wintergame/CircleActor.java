package at.mager.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor {
	private double x, y;

	public CircleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		this.y++;
		if (y >= 800) {
			this.y = 0;
		}
	}

	public void render(Graphics graphics) {
		graphics.fillOval((float) this.x, (float) this.y, 50, 50);
	}

}