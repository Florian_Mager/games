package at.mager.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Snowman {

	private float x,y,width,height;
	private Image snowman;

	public Snowman() throws SlickException {
		
		this.x = 200;
		this.y = 200;
		this.width = 150;
		this.height = 150;
		this.snowman = new Image("D:/Daten/HTL Dornbirn/SWP/SWP4/pictures/snowman.png");
		
	}
	
	public void update(GameContainer gc, int delta) {
		
	}

	public void render(Graphics graphics) {
		this.snowman.draw(this.x, this.y, this.width, this.height);
	}

}