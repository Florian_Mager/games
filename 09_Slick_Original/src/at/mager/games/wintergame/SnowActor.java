package at.mager.games.wintergame;

import java.util.Random;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class SnowActor {

	private float x, y, size, speed;
	private Random r;

	public SnowActor(int size) {

		this.r = new Random();
		this.x = r.nextInt(800);
		this.y = r.nextInt(600) * -1;

		if (size == 0) {
			this.size = 5;
			this.speed = 1;
		}

		if (size == 1) {
			this.size = 8;
			this.speed = 2;
		}

		if (size == 2) {
			this.size = 16;
			this.speed = 3;
		}

	}

	public void update(GameContainer gc, int delta) {
		this.y = this.y + (delta * this.speed * 0.10f);
		
		if (this.y >= 650) {
			this.x = r.nextInt(800);
			this.y = r.nextInt(600) * -1;
		}
	}

	public void render(Graphics graphics) {
		graphics.fillOval(this.x, this.y, this.size, this.size);

	}

}